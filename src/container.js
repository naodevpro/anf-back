import { createContainer, Lifetime, asClass, asValue } from 'awilix';

import express, { Router } from 'express';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import csurf from 'csurf';
import multer from 'multer';
import nodemailer from 'nodemailer';
import hbs from 'nodemailer-express-handlebars';
import path from 'path';

import { ApiError, handleError } from './helpers/error';
import responseHandler from './helpers/response';
import JwtService from './libs/JwtService';
import Mailing from './libs/Mailing';

import Server from './config/server';
import config from './config/environment';
import configCloudinary from './config/cloudinary';
import db from './config/database/models';

const stripe = require('stripe')(config.stripe_key);

const container = createContainer();
const jwtService = new JwtService(jwt, config.jwt_secret);
const mailing = new Mailing(nodemailer, hbs);

const router = Router();
const csrfMiddleware = csurf({ cookie: true });
const storage = multer.diskStorage({});
const upload = multer({ storage: storage });

container.register({
  config: asValue(config),
  bcrypt: asValue(bcrypt),
  jwt: asValue(jwt),
  db: asValue(db),
  cookieParser: asValue(cookieParser),
  csrfMiddleware: asValue(csrfMiddleware),
  ApiError: asValue(ApiError),
  handleError: asValue(handleError),
  jwtService: asValue(jwtService),
  responseHandler: asValue(responseHandler),
  upload: asValue(upload),
  configCloudinary: asValue(configCloudinary),
  stripe: asValue(stripe),
  mailing: asValue(mailing),
  nodemailer: asValue(nodemailer),
  express: asValue(express),
  cors: asValue(cors),
  router: asValue(router)
});

container.loadModules(
  [
    'modules/**/*!(Dao$).js',
    'middlewares/*!(index).js',
    'libs/*!(index).js',
    'helpers/*!(index).js'
  ],
  {
    resolverOptions: {
      lifetime: Lifetime.SINGLETON
    },
    cwd: __dirname
  }
);

container.loadModules(['modules/**/*Dao.js'], {
  resolverOptions: {
    lifetime: Lifetime.SINGLETON,
    register: asValue
  },
  cwd: __dirname
});

const routesName = Object.keys(container.registrations).filter((item) =>
  item.match(/Router$/g)
);

const routes = routesName.map((route) => container.resolve(route));

container.register({
  routes: asValue(routes),
  server: asClass(Server).singleton()
});

export default container;
