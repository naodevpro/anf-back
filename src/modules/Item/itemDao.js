'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Item extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.Objectif, {
        foreignKey: 'itemId',
        onDelete: 'CASCADE'
      });
      this.hasMany(models.Constat, {
        foreignKey: 'itemId',
        onDelete: 'CASCADE'
      });
    }
  }
  Item.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      title: DataTypes.TEXT,
      shortDescription: DataTypes.TEXT,
      headerAsset: DataTypes.TEXT,
      titleLongDescription: DataTypes.TEXT,
      longDescription: DataTypes.TEXT,
      citation: DataTypes.TEXT
    },
    {
      sequelize,
      modelName: 'Item'
    }
  );
  return Item;
};
