class ItemRepository {
  constructor({ db, ApiError }) {
    this.db = db;
    this.apiError = ApiError;
  }

  async getItems() {
    const items = await this.db.Item.findAll({
      required: true
    });
    if (!items) {
      throw new this.apiError(400, "Il semble qu'il n'y a aucun items 😖");
    }
    return items;
  }

  async getItemById(id) {
    const item = await this.db.Item.findOne({
      where: { id: id },
      include: [
        {
          model: this.db.Constat
        },
        {
          model: this.db.Objectif,
          include: [
            {
              model: this.db.Argument
            }
          ]
        }
      ]
    });
    if (!item) {
      throw new this.apiError(
        400,
        "Il semble qu'il n'y ai aucun item à cet ID😖"
      );
    }
    return item;
  }

  async createItem(item) {
    const itemExist = await this.db.Item.findOne({
      where: { title: item.title }
    });
    if (itemExist) {
      throw new this.apiError(
        400,
        "Il semble qu'il existe deja un items du même nom😖"
      );
    }
    return await this.db.Item.create(item);
  }

  async deleteItemById(id) {
    const itemExist = await this.db.Item.findOne({
      where: { id: id }
    });
    if (!itemExist) {
      throw new this.apiError(
        400,
        "Il semble que l'item que vous voulez supprimer n'existe pas/plus 😖"
      );
    }
    return await this.db.Item.destroy({ where: { id: id } });
  }

  async updateItemById(id, item) {
    const itemExist = await this.db.Item.findOne({
      where: { id: id }
    });
    if (!itemExist) {
      throw new this.apiError(
        400,
        "Il semble que l'item que vous voulez modifier n'existe pas/plus 😖"
      );
    }
    return await this.db.Item.update(
      {
        title: item.title,
        shortDescription: item.shortDescription,
        headerAsset: item.headerAsset
          ? item.headerAsset
          : itemExist.dataValues.headerAsset,
        titleLongDescription: item.bodytitleLongDescription,
        longDescription: item.longDescription,
        assetDescription: item.assetDescription,
        titleConstat: item.titleConstat,
        titleSubjectConstat: item.titleSubjectConstat,
        contentSubjectConstat: item.contentSubjectConstat,
        assetSubjectConstat: item.assetSubjectConstat,
        titleProposition: item.titleProposition,
        titleObjectif: item.titleObjectif,
        titleArgument: item.titleArgument,
        descriptionArgument: item.descriptionArgument,
        citation: item.citation
      },
      { where: { id: id } }
    );
  }
}
export default ItemRepository;
