export default class ItemRouter {
  constructor({ router, upload, auth, itemController }) {
    this.router = router;
    this.upload = upload;
    this.initializeRoutes({ itemController, auth });
    return this.router;
  }

  initializeRoutes({ itemController, auth }) {
    this.router.route('/items').get(itemController.getAllItems);
    this.router.route('/items/:id').get(itemController.getItem);
    this.router
      .route('/items')
      .post(this.upload.single('headerAsset'), itemController.addItem);
    this.router
      .route('/items/:id')
      .delete(auth.isAuthentificated, itemController.deleteItem);
    this.router
      .route('/items/:id')
      .patch(
        auth.isAuthentificated,
        this.upload.single('headerAsset'),
        itemController.updateItem
      );
  }
}
