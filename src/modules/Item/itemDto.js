class ItemDto {
  constructor({
    id,
    title,
    shortDescription,
    headerAsset,
    titleLongDescription,
    longDescription,
    citation,
    updatedAt,
    createdAt
  }) {
    this.id = id;
    this.title = title;
    this.shortDescription = shortDescription;
    this.headerAsset = headerAsset;
    this.titleLongDescription = titleLongDescription;
    this.longDescription = longDescription;
    this.citation = citation;
    this.updatedAt = updatedAt;
    this.createdAt = createdAt;
  }

  async mappingGetItem(item) {
    item = item.dataValues;

    const itemMapped = {
      id: item.id,
      title: item.title,
      shortDescription: item.shortDescription,
      headerAsset: item.headerAsset,
      titleLongDescription: item.titleLongDescription,
      longDescription: item.longDescription,
      constats: item.Constats
        ? item.Constats.map((constat) => {
            return {
              titleConstat: constat.dataValues.titleConstat,
              contentConstat: constat.dataValues.contentConstat,
              assetConstat: constat.dataValues.assetConstat,
              itemId: constat.dataValues.itemId
            };
          })
        : null,
      objectifs: item.Objectifs
        ? item.Objectifs.map((objectif) => {
            return {
              titleObjectif: objectif.dataValues.titleObjectif,
              descriptionObjectif: objectif.dataValues.descriptionObjectif,
              arguments: objectif.Arguments
                ? objectif.Arguments.map((argument) => {
                    return {
                      titleArgument: argument.dataValues.titleArgument,
                      descriptionArgument:
                        argument.dataValues.descriptionArgument
                    };
                  })
                : null
            };
          })
        : null,
      citation: item.citation
    };
    return await itemMapped;
  }

  async mappingGetAllItems(itemsReceived) {
    const items = [];
    itemsReceived.map((item) => {
      item = item.dataValues;
      items.push({
        id: item.id,
        title: item.title,
        headerAsset: item.headerAsset
      });
    });
    return await items;
  }
}

export default ItemDto;
