class ItemEntity {
  constructor({id, title, shortDescription, headerAsset, titleLongDescription, longDescription, citation}) {
    this.id = id;
    this.title = title;
    this.shortDescription = shortDescription;
    this.headerAsset = headerAsset;
    this.titleLongDescription = titleLongDescription;
    this.longDescription = longDescription;
    this.citation = citation;
  }

  validate() {
    if(!this.title, !this.shortDescription, !this.headerAsset, !this.titleLongDescription, !this.longDescription, !this.citation) {
      return false;
    } else {
      return true;
    }
  }
}

export default ItemEntity;
