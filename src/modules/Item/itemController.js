import ItemDto from './itemDto';

class ItemController {
  constructor({ itemService, configCloudinary, responseHandler, ApiError }) {
    this.itemService = itemService;
    this.configCloudinary = configCloudinary;
    this.responseHandler = responseHandler;
    this.ApiError = ApiError;
  }

  getAllItems = async (request, response, next) => {
    try {
      let itemsReceived = await this.itemService.getAllItems();
      const itemDto = new ItemDto(itemsReceived);
      const items = await itemDto.mappingGetAllItems(itemsReceived);
      this.responseHandler(response, 201, items, `Tous les items 🇫🇷💥`);
    } catch (err) {
      next(err);
    }
  };

  addItem = async (request, response, next) => {
    try {
      if (!request.file) {
        throw new this.ApiError(
          400,
          "Il semble que vous n'ayez pas ajouté d'image à l'item ❌ "
        );
      }
      const result = await this.configCloudinary.uploader.upload(
        request.file.path
      );
      if (!result) {
        throw new this.ApiError(
          400,
          "Il semble qu'il y ait une erreur lors de l'upload de l'image ❌ "
        );
      }
      let item = await this.itemService.addItem({
        title: request.body.title,
        shortDescription: request.body.shortDescription,
        headerAsset: result.secure_url,
        titleLongDescription: request.body.titleLongDescription,
        longDescription: request.body.longDescription,
        citation: request.body.citation
      });
      this.responseHandler(
        response,
        201,
        item,
        `Nouvel item ajouté ! Vive la France ! 🇫🇷💥`
      );
    } catch (err) {
      next(err);
    }
  };

  getItem = async (request, response, next) => {
    try {
      let itemReceived = await this.itemService.getItem(request.params.id);
      const itemDto = new ItemDto(itemReceived);
      const item = await itemDto.mappingGetItem(itemReceived);
      this.responseHandler(response, 201, item);
    } catch (err) {
      next(err);
    }
  };

  deleteItem = async (request, response, next) => {
    try {
      let itemDeleted = await this.itemService.deleteItem(request.params.id);
      this.responseHandler(response, 200, itemDeleted, 'Item supprimé ✅');
    } catch (err) {
      next(err);
    }
  };

  updateItem = async (request, response, next) => {
    try {
      let result;
      if (request.file) {
        result = await this.configCloudinary.uploader.upload(request.file.path);
        result = result.secure_url;
      } else {
        result = null;
      }
      let itemUpdated = await this.itemService.updateItem(request.params.id, {
        title: request.body.title,
        shortDescription: request.body.shortDescription,
        headerAsset: result,
        titleLongDescription: request.bodytitleLongDescription,
        longDescription: request.body.longDescription,
        assetDescription: request.body.assetDescription,
        titleConstat: request.body.titleConstat,
        titleSubjectConstat: request.body.titleSubjectConstat,
        contentSubjectConstat: request.bodycontentSubjectConstat,
        assetSubjectConstat: request.body.assetSubjectConstat,
        titleProposition: request.body.titleProposition,
        titleObjectif: request.body.titleObjectif,
        titleArgument: request.body.titleArgument,
        descriptionArgument: request.body.descriptionArgument,
        citation: request.body.citation
      });
      this.responseHandler(response, 200, itemUpdated, 'Item mis à jour ✅');
    } catch (err) {
      next(err);
    }
  };
}

export default ItemController;
