
class ItemService {
  constructor({ itemRepository, ApiError }) {
    this.itemRepository = itemRepository;
    this.apiError = ApiError;
  }

  async getAllItems() {
    return await this.itemRepository.getItems();
  }

  async getItem(id) {
    return await this.itemRepository.getItemById(id);
  }

  async addItem(item) {
    return await this.itemRepository.createItem(item);
  }

  async deleteItem(id) {
    return await this.itemRepository.deleteItemById(id);
  }

  async updateItem(id, item) {
    return await this.itemRepository.updateItemById(id, item);
  }
}

export default ItemService;
