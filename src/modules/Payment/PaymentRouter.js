export default class PaymentRouter {
  constructor({ router, stripe, responseHandler }) {
    this.router = router;
    this.stripe = stripe;
    this.responseHandler = responseHandler;
    this.initializeRoutes();
    return this.router;
  }

  initializeRoutes() {
    this.router.route('/payment').get((request, response, next) => {
      response.json({
        message: 'Route de paiement 👋💥'
      });
    });
    this.router
      .route('/stripe/payment')
      .post(async (request, response, next) => {
        await this.stripe.charges.create(
          {
            amount: 200,
            currency: 'EUR',
            description: 'One time setup fee',
            source: request.body.token
          },
          (err, charge) => {
            if (err) {
              next(err);
            }
            this.responseHandler(response, 200, 'Paiement effectué');
          }
        );
      });
  }
}
