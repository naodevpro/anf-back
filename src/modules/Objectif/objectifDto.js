class ObjectifDto {
  constructor({ id, titleObjectif, descriptionObjectif, itemId }) {
    this.id = id;
    this.titleObjectif = titleObjectif;
    this.descriptionObjectif = descriptionObjectif;
    this.itemId = itemId;
  }

  async mappingGetObjectif() {
    const objectif = {
      id: this.id,
      titleObjectif: this.titleObjectif,
      descriptionObjectif: this.descriptionObjectif,
      itemId: this.itemId
    };
    return await objectif;
  }

  async mappingGetAllObjectifs(objectifsReceived) {
    const objectifs = [];
    objectifsReceived.map((objectif) => {
      objectifs.push({
        id: objectif.id,
        titleObjectif: objectif.titleObjectif,
        descriptionObjectif: objectif.descriptionObjectif,
        itemId: objectif.itemId
      });
    });
    return await objectifs;
  }
}

export default ObjectifDto;
