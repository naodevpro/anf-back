class ObjectifRouter {
  constructor({ router, auth, objectifController }) {
    this.router = router;
    this.initializeRoutes({ objectifController });
    return this.router;
  }

  initializeRoutes({ objectifController }) {
    this.router.route('/objectifs').post(objectifController.addObjectif);
    this.router
      .route('/objectifs/:id')
      .patch(objectifController.updateObjectif);
    this.router
      .route('/objectifs/:id')
      .delete(objectifController.deleteObjectif);
    this.router.route('/objectifs').get(objectifController.getAllObjectifs);
    this.router.route('/objectifs/:id').get(objectifController.getObjectifById);
  }
}
export default ObjectifRouter;
