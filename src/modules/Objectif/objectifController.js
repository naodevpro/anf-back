import ObjectifDto from './objectifDto';

class ObjectifController {
  constructor({ objectifService, responseHandler }) {
    this.objectifService = objectifService;
    this.responseHandler = responseHandler;
  }
  addObjectif = async (request, response, next) => {
    try {
      let objectif = await this.objectifService.addObjectif({
        titleObjectif: request.body.titleObjectif,
        descriptionObjectif: request.body.descriptionObjectif,
        itemId: request.body.itemId
      });
      this.responseHandler(
        response,
        201,
        objectif,
        `Nouvel objectif ajouté dans la base de donées!💥`
      );
    } catch (err) {
      next(err);
    }
  };

  deleteObjectif = async (request, response, next) => {
    try {
      let objectifDeleted = await this.objectifService.deleteObjectif(
        request.params.id
      );
      this.responseHandler(
        response,
        200,
        objectifDeleted,
        'objectif supprimé ✅'
      );
    } catch (err) {
      next(err);
    }
  };

  updateObjectif = async (request, response, next) => {
    try {
      let objectifUpdated = await this.objectifService.updateObjectif(
        request.params.id,
        {
          titleObjectif: request.body.titleObjectif,
          descriptionObjectif: request.body.descriptionObjectif
        }
      );
      this.responseHandler(
        response,
        200,
        objectifUpdated,
        'Objectif mis à jour ✅'
      );
    } catch (err) {
      next(err);
    }
  };

  getAllObjectifs = async (request, response, next) => {
    try {
      let objectifsReceived = await this.objectifService.getAllObjectifs();
      const objectifDto = new ObjectifDto(objectifsReceived);
      const objectifs = await objectifDto.mappingGetAllObjectifs(
        objectifsReceived
      );
      this.responseHandler(response, 200, objectifs, 'Tous les objectifs ✅');
    } catch (err) {
      next(err);
    }
  };

  getObjectifById = async (request, response, next) => {
    try {
      let objectifReceived = await this.objectifService.getObjectif(
        request.params.id
      );
      const objectifDto = new ObjectifDto(objectifReceived);
      const objectif = await objectifDto.mappingGetObjectif(objectifReceived);
      this.responseHandler(response, 200, objectif);
    } catch (err) {
      next(err);
    }
  };
}

export default ObjectifController;
