class objectifEntity {
  constructor({ id, titleObjectif, descriptionObjectif, itemId }) {
    this.id = id;
    this.titleObjectif = titleObjectif;
    this.descriptionObjectif = descriptionObjectif;
    this.itemId = itemId;
  }

  validate() {
    if (!this.titleObjectif || !this.descriptionObjectif || !this.itemId) {
      return false;
    } else {
      return true;
    }
  }
}

export default objectifEntity
