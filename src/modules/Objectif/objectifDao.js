'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Objectif extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.Item, {
        foreignKey: {
          allowNull: false,
          name: 'itemId'
        },
        as: 'items'
      });
      this.hasMany(models.Argument, {
        foreignKey: 'objectifId',
        onDelete: 'CASCADE'
      });
    }
  }
  Objectif.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      titleObjectif: DataTypes.TEXT,
      descriptionObjectif: DataTypes.TEXT,
      itemId: DataTypes.UUID
    },
    {
      sequelize,
      modelName: 'Objectif'
    }
  );
  return Objectif;
};
