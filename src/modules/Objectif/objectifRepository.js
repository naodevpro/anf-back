class ObjectifRepository {
  constructor({ db, ApiError }) {
    this.db = db;
    this.apiError = ApiError;
  }

  async createObjectif(objectif) {
    const objectifExist = await this.db.Objectif.findOne({
      where: { titleObjectif: objectif.titleObjectif }
    });
    if (objectifExist) {
      throw new this.apiError(
        400,
        "Il semble qu'il existe deja un objectif du même nom 😖"
      );
    }
    return await this.db.Objectif.create(objectif);
  }

  async deleteObjectifById(id) {
    const objectifExist = await this.db.Objectif.findOne({
      where: { id: id }
    });
    if (!objectifExist) {
      throw new this.apiError(
        400,
        "Il semble que l'objectif que vous voulez supprimer n'existe pas/plus 😖"
      );
    }
    return await this.db.Objectif.destroy({ where: { id: id } });
  }

  async updateObjectifById(id, objectif) {
    const objectifExist = await this.db.Objectif.findOne({
      where: { id: id }
    });
    if (!objectifExist) {
      throw new this.apiError(
        400,
        "Il semble que l'objectif que vous voulez modifier n'existe pas/plus 😖"
      );
    }
    return await this.db.Objectif.update(
      {
        titleObjectif: objectif.titleObjectif,
        descriptionObjectif: objectif.descriptionObjectif
      },
      { where: { id: id } }
    );
  }

  async getObjectifs() {
    const objectifs = await this.db.Objectif.findAll({
      attributes: ['id', 'titleObjectif', 'descriptionObjectif', 'itemId']
    });
    if (!objectifs) {
      throw new this.apiError(400, 'Aucun objectifs enregistrés');
    }
    return objectifs;
  }

  async getObjectifById(id) {
    const objectif = await this.db.Objectif.findOne({
      attributes: ['id', 'titleObjectif', 'descriptionObjectif', 'itemId'],
      where: { id: id }
    });
    if (!objectif) {
      throw new this.apiError(
        400,
        "Il semble qu'il n'y ai aucun objectif à cet ID ❌"
      );
    }
    return objectif;
  }
}
export default ObjectifRepository;
