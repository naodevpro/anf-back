class ObjectifService {
  constructor({ objectifRepository, ApiError }) {
    this.objectifRepository = objectifRepository;
    this.apiError = ApiError;
  }

  async addObjectif(objectif) {
    return await this.objectifRepository.createObjectif(objectif);
  }

  async deleteObjectif(id) {
    return await this.objectifRepository.deleteObjectifById(id);
  }

  async updateObjectif(id, objectif) {
    return await this.objectifRepository.updateObjectifById(id, objectif);
  }

  async getAllObjectifs() {
    return await this.objectifRepository.getObjectifs();
  }

  async getObjectif(id) {
    return await this.objectifRepository.getObjectifById(id);
  }
}

export default ObjectifService;
