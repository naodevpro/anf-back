class ConstatService {
  constructor({ constatRepository, ApiError }) {
    this.constatRepository = constatRepository;
    this.apiError = ApiError;
  }

  async addConstat(constat) {
    return await this.constatRepository.createConstat(constat);
  }

  async deleteConstat(id) {
    return await this.constatRepository.deleteConstatById(id);
  }

  async updateConstat(id, constat) {
    return await this.constatRepository.updateConstatById(id, constat);
  }

  async getAllConstats() {
    return await this.constatRepository.getConstats();
  }

  async getConstat(id) {
    return await this.constatRepository.getConstatById(id);
  }
}

export default ConstatService;
