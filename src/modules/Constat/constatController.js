import ConstatDto from './constatDto';

class ConstatController {
  constructor({ constatService, configCloudinary, responseHandler, ApiError }) {
    this.constatService = constatService;
    this.configCloudinary = configCloudinary;
    this.responseHandler = responseHandler;
    this.ApiError = ApiError;
  }
  addConstat = async (request, response, next) => {
    try {
      if (!request.file) {
        throw new this.ApiError(
          400,
          "Il semble que vous n'ayez pas ajouté d'image au constat ❌ "
        );
      }
      const result = await this.configCloudinary.uploader.upload(
        request.file.path
      );
      if (!result) {
        throw new this.ApiError(
          400,
          "Il semble qu'il y ait une erreur lors de l'upload de l'image ❌ "
        );
      }
      let constatReceived = await this.constatService.addConstat({
        titleConstat: request.body.titleConstat,
        contentConstat: request.body.contentConstat,
        assetConstat: result.secure_url,
        itemId: request.body.itemId
      });
      const constatDto = new ConstatDto(constatReceived.dataValues);
      const constat = await constatDto.mappingAddConstat();
      this.responseHandler(
        response,
        201,
        constat,
        `Nouvel constat ajouté dans la base de donées!💥`
      );
    } catch (err) {
      next(err);
    }
  };

  deleteConstat = async (request, response, next) => {
    try {
      let constatDeleted = await this.constatService.deleteConstat(
        request.params.id
      );
      this.responseHandler(
        response,
        200,
        constatDeleted,
        'Constat supprimé ✅'
      );
    } catch (err) {
      next(err);
    }
  };

  updateConstat = async (request, response, next) => {
    try {
      let constatUpdated = await this.constatService.updateConstat(
        request.params.id,
        {
          titleConstat: request.body.titleConstat,
          titleSubjectConstat: request.body.titleSubjectConstat,
          assetSubjectConstat: request.body.assetSubjectConstat
        }
      );
      this.responseHandler(
        response,
        200,
        constatUpdated,
        'Constat mis à jour ✅'
      );
    } catch (err) {
      next(err);
    }
  };

  getAllConstats = async (request, response, next) => {
    try {
      let constatsReceived = await this.constatService.getAllConstats();
      const constatDto = new ConstatDto(constatsReceived);
      const constats = await constatDto.mappingGetAllConstats(constatsReceived);
      this.responseHandler(response, 200, constats, 'Tous les constats ✅');
    } catch (err) {
      next(err);
    }
  };

  getConstatById = async (request, response, next) => {
    try {
      let constatReceived = await this.constatService.getConstat(
        request.params.id
      );
      const constatDto = new ConstatDto(constatReceived);
      const constat = await constatDto.mappingAddConstat(constatReceived);
      this.responseHandler(response, 200, constat);
    } catch (err) {
      next(err);
    }
  };
}

export default ConstatController;
