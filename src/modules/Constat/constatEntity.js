class constatEntity {
  constructor({ id, titleConstat, contentConstat, assetConstat, itemId }) {
    this.id = id;
    this.titleConstat = titleConstat;
    this.contentConstat = contentConstat;
    this.assetConstat = assetConstat;
    this.itemId = itemId;
  }

  validate() {
    if (!this.titleConstat || !this.contentConstat || !this.itemId) {
      return false;
    } else {
      return true;
    }
  }
}

export default constatEntity;
