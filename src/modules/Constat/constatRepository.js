class ConstatRepository {
  constructor({ db, ApiError }) {
    this.db = db;
    this.apiError = ApiError;
  }

  async createConstat(constat) {
    const constatExist = await this.db.Constat.findOne({
      where: { titleConstat: constat.titleConstat }
    });
    if (constatExist) {
      throw new this.apiError(
        400,
        "Il semble qu'il existe deja un constat du même nom 😖"
      );
    }
    return await this.db.Constat.create(constat);
  }

  async deleteConstatById(id) {
    const constatExist = await this.db.Constat.findOne({
      where: { id: id }
    });
    if (!constatExist) {
      throw new this.apiError(
        400,
        "Il semble que le constat que vous voulez supprimer n'existe pas/plus 😖"
      );
    }
    return await this.db.Constat.destroy({ where: { id: id } });
  }

  async updateConstatById(id, constat) {
    const constatExist = await this.db.Constat.findOne({
      where: { id: id }
    });
    if (!constatExist) {
      throw new this.apiError(
        400,
        "Il semble que le constat que vous voulez modifier n'existe pas/plus 😖"
      );
    }
    return await this.db.Constat.update(
      {
        titleConstat: constat.titleConstat,
        titleSubjectConstat: constat.titleSubjectConstat,
        assetSubjectConstat: constat.assetSubjectConstat
      },
      { where: { id: id } }
    );
  }

  async getConstats() {
    const constats = await this.db.Constat.findAll({
      attributes: [
        'id',
        'titleConstat',
        'contentConstat',
        'assetConstat',
        'itemId'
      ]
    });
    if (!constats) {
      throw new this.apiError(400, 'Aucun constats enregistrés');
    }
    return constats;
  }

  async getConstatById(id) {
    const constat = await this.db.Constat.findOne({
      attributes: [
        'id',
        'titleConstat',
        'contentConstat',
        'assetConstat',
        'itemId'
      ],
      where: { id: id }
    });
    if (!constat) {
      throw new this.apiError(
        400,
        "Il semble qu'il n'y ai aucun constat à cet ID ❌"
      );
    }
    return constat;
  }
}
export default ConstatRepository;
