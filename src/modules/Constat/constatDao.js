'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Constat extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.Item, {
        constraints: true,
        onDelete: 'CASCADE',
        foreignKey: {
          allowNull: false,
          name: 'itemId'
        },
        as: 'items'
      });
    }
  }
  Constat.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      titleConstat: DataTypes.TEXT,
      contentConstat: DataTypes.TEXT,
      assetConstat: DataTypes.TEXT,
      itemId: DataTypes.UUID
    },
    {
      sequelize,
      modelName: 'Constat'
    }
  );
  return Constat;
};
