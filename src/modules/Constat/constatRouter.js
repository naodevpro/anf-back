class ConstatRouter {
  constructor({ router, auth, upload, constatController }) {
    this.router = router;
    this.upload = upload;
    this.initializeRoutes({ constatController });
    return this.router;
  }

  initializeRoutes({ constatController }) {
    this.router
      .route('/constats')
      .post(this.upload.single('assetConstat'), constatController.addConstat);
    this.router.route('/constats/:id').patch(constatController.updateConstat);
    this.router.route('/constats/:id').delete(constatController.deleteConstat);
    this.router.route('/constats').get(constatController.getAllConstats);
    this.router.route('/constats/:id').get(constatController.getConstatById);
  }
}
export default ConstatRouter;
