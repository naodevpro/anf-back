class ConstatDto {
  constructor({ id, titleConstat, contentConstat, assetConstat, itemId }) {
    this.id = id;
    this.titleConstat = titleConstat;
    this.contentConstat = contentConstat;
    this.assetConstat = assetConstat;
    this.itemId = itemId;
  }

  async mappingAddConstat() {
    const constatMapped = {
      titleConstat: this.titleConstat,
      contentConstat: this.contentConstat,
      assetConstat: this.assetConstat,
      itemId: this.itemId
    };
    return await constatMapped;
  }

  async mappingGetAllConstats(constatsReceived) {
    const constats = [];
    constatsReceived.map((constat) => {
      constats.push({
        titleConstat: constat.titleConstat,
        contentConstat: constat.contentConstat,
        assetConstat: constat.assetConstat,
        itemId: constat.itemId
      });
    });
    return await constats;
  }
}

export default ConstatDto;
