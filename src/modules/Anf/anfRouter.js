class AnfRouter {
  constructor({ router }) {
    this.router = router;
    this.initializeRoutes();
    return this.router;
  }

  initializeRoutes() {
    this.router.route('/').get((request, response, next) => {
      response.json({
        message:
          "Bienvenue dans l'API de l'Alliance pour une Nouvelle France  🇫🇷👋💥"
      });
    });
  }
}
export default AnfRouter;
