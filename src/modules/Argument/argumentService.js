class ArgumentService {
    constructor({ argumentRepository, ApiError }) {
      this.argumentRepository = argumentRepository;
      this.apiError = ApiError;
    }
  
    async addArgument(argument) {
      return await this.argumentRepository.createArgument(argument);
    }
  
    async deleteArgument(id) {
      return await this.argumentRepository.deleteArgumentById(id);
    }
  
    async updateArgument(id, argument) {
      return await this.argumentRepository.updateArgumentById(id, argument);
    }

    async getArguments() {
      return await this.argumentRepository.getArguments();
    }

    async getArgumentById(id) {
      return await this.argumentRepository.getArgumentById(id);
    }
  }
  
  export default ArgumentService;
  