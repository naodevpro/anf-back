class ArgumentRepository {
  constructor({ db, ApiError }) {
    this.db = db;
    this.apiError = ApiError;
  }

  async createArgument(argument) {
    const argumentExist = await this.db.Argument.findOne({
      where: { titleArgument: argument.titleArgument }
    });
    if (argumentExist) {
      throw new this.apiError(
        400,
        "Il semble qu'il existe deja un argument du même nom 😖"
      );
    }
    return await this.db.Argument.create(argument);
  }

  async deleteArgumentById(id) {
    const argumentExist = await this.db.Argument.findOne({
      where: { id: id }
    });
    if (!argumentExist) {
      throw new this.apiError(
        400,
        "Il semble que l'argument que vous voulez supprimer n'existe pas/plus 😖"
      );
    }
    return await this.db.Argument.destroy({ where: { id: id } });
  }

  async updateArgumentById(id, argument) {
    const argumentExist = await this.db.Argument.findOne({
      where: { id: id }
    });
    if (!argumentExist) {
      throw new this.apiError(
        400,
        "Il semble que l'argument que vous voulez modifier n'existe pas/plus 😖"
      );
    }
    return await this.db.Argument.update(
      {
        titleArgument: argument.titleArgument,
        descriptionArgument: argument.descriptionArgument
      },
      { where: { id: id } }
    );
  }

  async getArguments() {
    const argumentsFounded = await this.db.Argument.findAll({
      attributes: ['id', 'titleArgument', 'descriptionArgument', 'objectifId']
    });
    if (!argumentsFounded) {
      throw new this.apiError(400, 'Aucun arguments enregistrés');
    }
    return argumentsFounded;
  }

  async getArgumentById(id) {
    const argumentFounded = await this.db.Argument.findOne({
      attributes: ['id', 'titleArgument', 'descriptionArgument', 'objectifId'],
      where: { id: id }
    });
    if (!argumentFounded) {
      throw new this.apiError(
        400,
        "Il semble qu'il n'y ai aucun argument à cet ID ❌"
      );
    }
    return argumentFounded;
  }
}
export default ArgumentRepository;
