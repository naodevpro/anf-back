class ArgumentEntity {
  constructor({ id, titleArgument, descriptionArgument, objectifId }) {
    this.id = id;
    this.titleArgument = titleArgument;
    this.descriptionArgument = descriptionArgument;
    this.objectifId = objectifId;
  }

  validate() {
    if (!this.titleArgument || !this.descriptionArgument || !this.objectifId) {
      return false;
    } else {
      return true;
    }
  }
}

export default ArgumentEntity;
