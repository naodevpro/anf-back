class ArgumentRouter {
  constructor({ router, argumentController }) {
    this.router = router;
    this.initializeRoutes({ argumentController });
    return this.router;
  }

  initializeRoutes({ argumentController }) {
    this.router.route('/arguments').post(argumentController.addArgument);
    this.router
      .route('/arguments/:id')
      .patch(argumentController.updateArgument);
    this.router
      .route('/arguments/:id')
      .delete(argumentController.deleteArgument);
      this.router.route('/arguments').get(argumentController.getAllArguments);
      this.router.route('/arguments/:id').get(argumentController.getArgumentById);
  }
}
export default ArgumentRouter;
