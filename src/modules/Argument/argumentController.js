import ArgumentDto from './argumentDto';

class ArgumentController {
  constructor({ argumentService, responseHandler }) {
    this.argumentService = argumentService;
    this.responseHandler = responseHandler;
  }
  addArgument = async (request, response, next) => {
    try {
      let argument = await this.argumentService.addArgument({
        titleArgument: request.body.titleArgument,
        descriptionArgument: request.body.descriptionArgument,
        objectifId: request.body.objectifId
      });
      this.responseHandler(
        response,
        201,
        argument,
        `Nouvel argument ajouté dans la base de données!💥`
      );
    } catch (err) {
      next(err);
    }
  };

  deleteArgument = async (request, response, next) => {
    try {
      let argumentDeleted = await this.argumentService.deleteArgument(
        request.params.id
      );
      this.responseHandler(
        response,
        200,
        argumentDeleted,
        'Argument supprimé ✅'
      );
    } catch (err) {
      next(err);
    }
  };

  updateArgument = async (request, response, next) => {
    try {
      let argumentUpdated = await this.argumentService.updateArgument(
        request.params.id,
        {
          titleArgument: request.body.titleArgument,
          descriptionArgument: request.body.descriptionArgument
        }
      );
      this.responseHandler(
        response,
        200,
        argumentUpdated,
        'Argument mis à jour ✅'
      );
    } catch (err) {
      next(err);
    }
  };

  getAllArguments = async (request, response, next) => {
    try {
      let argumentsReceived = await this.argumentService.getArguments();
      const argumentDto = new ArgumentDto(argumentsReceived);
      const argumentsMapped = await argumentDto.mappingGetAllArguments(
        argumentsReceived
      );
      this.responseHandler(response, 200, argumentsMapped, 'Tous les arguments ✅');
    } catch (err) {
      next(err);
    }
  };

  getArgumentById = async (request, response, next) => {
    try {
      let argumentReceived = await this.argumentService.getArgumentById(request.params.id);
      const argumentDto = new ArgumentDto(argumentReceived);
      const argument = await argumentDto.mappingGetArgument(argumentReceived);
      this.responseHandler(response, 200, argument);
    } catch (err) {
      next(err);
    }
  }
}

export default ArgumentController;
