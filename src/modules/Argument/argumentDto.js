class ArgumentDto {
  constructor({ id, titleArgument, descriptionArgument, objectifId }) {
    this.id = id;
    this.titleArgument = titleArgument;
    this.descriptionArgument = descriptionArgument;
    this.objectifId = objectifId;
  }

  async mappingGetArgument() {
    const argument = {
      id: this.id,
      titleArgument: this.titleArgument,
      descriptionArgument: this.descriptionArgument,
      objectifId: this.objectifId
    };
    return await argument;
  }

  async mappingGetAllArguments(argumentsReceived) {
    const argumentsMapped = [];
    argumentsReceived.map((argument) => {
      argumentsMapped.push({
        id: argument.id,
        titleArgument: argument.titleArgument,
        descriptionArgument: argument.descriptionArgument,
        objectifId: argument.objectifId
      });
    });
    return await argumentsMapped;
  }
}

export default ArgumentDto;
