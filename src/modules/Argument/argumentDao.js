'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Argument extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.Objectif, {
        constraints: true,
        onDelete: 'CASCADE',
        foreignKey: {
          allowNull: false,
          name: 'objectifId'
        },
        as: 'objectifs'
      });
    }
  }
  Argument.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      titleArgument: DataTypes.TEXT,
      descriptionArgument: DataTypes.TEXT,
      objectifId: DataTypes.UUID
    },
    {
      sequelize,
      modelName: 'Argument'
    }
  );
  return Argument;
};
