class DonationRepository {
  constructor({ db, ApiError }) {
    this.db = db;
    this.apiError = ApiError;
  }

  async createDonation(userDonator) {
    let userExisted = await this.db.User.findOne({
      where: { email: userDonator.email }
    });
    if (userExisted) {
      return await this.db.Donation.create({
        userId: userExisted.dataValues.id,
        nationality: userDonator.nationality,
        adress: userDonator.adress,
        birthdate: userDonator.birthdate,
        gender: userDonator.gender,
        phone: userDonator.phone,
        elected: userDonator.elected,
        newsletter: userDonator.newsletter,
        newsletterSms: userDonator.newsletterSms,
        amount: userDonator.amount
      });
    }
    let newUserDonator = await this.db.User.create({
      role: 'donator',
      lastname: userDonator.lastname,
      firstname: userDonator.firstname,
      email: userDonator.email
    });
    return await this.db.Donation.create({
      userId: newUserDonator.dataValues.id,
      nationality: userDonator.nationality,
      adress: userDonator.adress,
      birthdate: userDonator.birthdate,
      gender: userDonator.gender,
      phone: userDonator.phone,
      elected: userDonator.elected,
      newsletter: userDonator.newsletter,
      newsletterSms: userDonator.newsletterSms,
      amount: userDonator.amount
    });
  }

  async getAllDonations() {
    return await this.db.Donation.findAll();
  }
}

export default DonationRepository;
