'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Donation extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.User, {
        foreignKey: 'userId'
      });
    }
  }
  Donation.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      userId: DataTypes.UUID,
      nationality: DataTypes.TEXT,
      adress: DataTypes.TEXT,
      birthdate: DataTypes.TEXT,
      gender: DataTypes.STRING,
      phone: DataTypes.INTEGER,
      elected: DataTypes.BOOLEAN,
      newsletter: DataTypes.BOOLEAN,
      newsletterSms: DataTypes.BOOLEAN,
      amount: DataTypes.INTEGER
    },
    {
      sequelize,
      modelName: 'Donation'
    }
  );
  return Donation;
};
