class DonationEntity {
  constructor({
    email,
    nationality,
    adress,
    birthdate,
    gender,
    phone,
    elected,
    newsletter,
    newsletterSms,
    amount
  }) {
    this.email = email;
    this.nationality = nationality;
    this.adress = adress;
    this.birthdate = birthdate;
    this.gender = gender;
    this.phone = phone;
    this.elected = elected;
    this.newsletter = newsletter;
    this.newsletterSms = newsletterSms;
    this.amount = amount;
  }

  checkPhoneNumber(phone) {
    const regexp = new RegExp(`(0|\\+33|0033)[1-9][0-9]{8}`);
    if (!regexp.test(phone)) {
      return false;
    }
    return true;
  }

  checkEmail(email) {
    const emailRegexp =
      /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (!emailRegexp.test(email)) {
      return false;
    } else {
      return true;
    }
  }

  checkAmount(amount) {
    if (amount < 1 || amount > 4600) {
      return false;
    }
    return true;
  }

  checkGender(gender) {
    if (gender === 'female' || gender === 'masculin' || gender === 'other') {
      return true;
    }
    return false;
  }
}

export default DonationEntity;
