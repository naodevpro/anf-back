class DonationDto {
  constructor({ id, userId, amount }) {
    this.id = id;
    this.userId = userId;
    this.amount = amount;
  }
}

export default DonationDto;
