import DonationEntity from './donationEntity';

class DonationService {
  constructor({ donationRepository, ApiError }) {
    this.donationRepository = donationRepository;
    this.apiError = ApiError;
  }

  async addDonation(userDonator) {
    const donationEntity = new DonationEntity({ userDonator });
    if (!donationEntity.checkEmail(userDonator.email)) {
      throw new this.apiError(
        400,
        'Veuillez rentrer une adresse e-mail valide ! ❌'
      );
    }
    if (!donationEntity.checkPhoneNumber(userDonator.phone)) {
      throw new this.apiError(
        400,
        'Veuillez rentrer un numéro de téléphone valide ! ❌'
      );
    }
    if (!donationEntity.checkAmount(userDonator.amount)) {
      throw new this.apiError(
        400,
        'Veuillez rentrer un montant au dessus de 1€ et en dessous de 7500€ ! ❌'
      );
    }
    if (!donationEntity.checkGender(userDonator.gender)) {
      throw new this.apiError(
        400,
        'Veuillez rentrer une selection valide pour le genre ! ❌'
      );
    }
    return await this.donationRepository.createDonation(userDonator);
  }

  async getDonations() {
    return await this.donationRepository.getAllDonations();
  }
}
export default DonationService;
