class DonationController {
  constructor({ donationService, stripe, mailing, responseHandler }) {
    this.donationService = donationService;
    this.stripe = stripe;
    this.mailing = mailing;
    this.responseHandler = responseHandler;
  }

  addDonation = async (request, response, next) => {
    try {
      await this.stripe.charges.create(
        {
          amount: request.body.amount * 100,
          currency: 'EUR',
          description: 'One time setup fee',
          source: request.body.token
        },
        async (err, charge) => {
          if (err) {
            next(err);
          }
          let donation = await this.donationService.addDonation({
            ...request.body
          });
          this.responseHandler(
            response,
            201,
            donation,
            'Un nouveau donateur à été ajouté ! 😉'
          );
          return await this.mailing.generateEmail(
            request.body.email,
            'Confirmation de votre donation',
            'donation',
            {
              id: donation.dataValues.id,
              firstname: request.body.firstname,
              amount: request.body.amount
            }
          );
        }
      );
    } catch (err) {
      next(err);
    }
  };

  getDonations = async (request, response, next) => {
    try {
      let donations = await this.donationService.getDonations();
      this.responseHandler(
        response,
        201,
        donations,
        'Toutes les donations! 😉'
      );
    } catch (err) {
      next(err);
    }
  };
}

export default DonationController;
