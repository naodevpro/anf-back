class DonationRouter {
  constructor({ router, donationController }) {
    this.router = router;
    this.initializeRoutes({ donationController });
    return this.router;
  }

  initializeRoutes({ donationController }) {
    this.router.route('/donations').post(donationController.addDonation);
    this.router.route('/donations').get(donationController.getDonations);
  }
}
export default DonationRouter;
