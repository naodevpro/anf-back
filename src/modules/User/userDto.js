class UserDto {
  constructor({ id, role, lastname, firstname, email, token }) {
    this.id = id;
    this.role = role;
    this.lastname = lastname;
    this.firstname = firstname;
    this.email = email;
    this.token = token;
  }

  async mappinGetUser() {
    const user = {
      id: this.id,
      role: this.role,
      lastname: this.lastname,
      firstname: this.firstname,
      email: this.email
    };
    return await user;
  }

  async mappingGetAllUsers(usersReceived) {
    const users = [];
    usersReceived.map((user) => {
      users.push({
        id: user.id,
        role: user.role,
        lastname: user.lastname,
        firstname: user.firstname,
        email: user.email
      });
    });
    return await users;
  }
}

export default UserDto;
