import UserDto from './userDto';

class UserController {
  constructor({ userService, jwtService, mailing, responseHandler }) {
    this.userService = userService;
    this.jwt = jwtService;
    this.mailing = mailing;
    this.responseHandler = responseHandler;
  }

  register = async (request, response, next) => {
    try {
      let user = await this.userService.register({ ...request.body });
      let token = await this.jwt.generateToken({
        id: user.dataValues.id,
        role: user.dataValues.role,
        lastname: user.dataValues.lastname,
        firstname: user.dataValues.firstname,
        email: user.dataValues.email
      });
      response.cookie('auth-cookie', token, {
        httpOnly: false,
        secure: false,
        maxAge: 3600000
      });
      this.responseHandler(
        response,
        201,
        user,
        `Bienvenue ${user.dataValues.firstname} 💥`
      );
    } catch (err) {
      next(err);
    }
  };

  login = async (request, response, next) => {
    try {
      let user = await this.userService.login(
        request.body.email,
        request.body.password
      );
      let token = await this.jwt.generateToken({
        id: user.dataValues.id,
        email: user.dataValues.email,
        role: user.dataValues.role
      });
      response.cookie('auth-cookie', token, {
        secure: true,
        maxAge: 3600000,
        sameSite: 'None'
      });
      this.responseHandler(
        response,
        200,
        {
          id: user.dataValues.id,
          lastname: user.dataValues.lastname,
          firstname: user.dataValues.firstname,
          email: user.dataValues.email,
          role: user.dataValues.role,
          token
        },
        `Bonjour ${user.dataValues.firstname} 💥`
      );
    } catch (err) {
      next(err);
    }
  };

  logout = async (request, response, next) => {
    try {
      response.clearCookie('auth-cookie', {
        sameSite: 'none',
        httpOnly: false,
        secure: true
      });
      this.responseHandler(response, 200, {}, 'Utilisateur déconnecté 🔐');
    } catch (err) {
      next(err);
    }
  };

  me = async (request, response, next) => {
    try {
      const token = await this.jwt.decodeToken(request.cookies['auth-cookie']);
      const user = await this.userService.me(token.id);
      this.responseHandler(
        response,
        200,
        user,
        "L'utilisateur est bien connecté ✅ "
      );
    } catch (err) {
      next(err);
    }
  };

  updateUser = async (request, response, next) => {
    try {
      let userUpdated = this.userService.updateUser(request.params.id, {
        role: request.body.role,
        lastname: request.body.lastname,
        firstname: request.body.firstname,
        email: request.body.email
      });
      this.responseHandler(
        response,
        200,
        userUpdated,
        "L'utilisateur a été mis à jour ✅"
      );
    } catch (err) {
      next(err);
    }
  };

  deleteUser = async (request, response, next) => {
    try {
      let userDeleted = this.userService.deleteUser(request.params.id);
      this.responseHandler(
        response,
        200,
        userDeleted,
        "L'utilisateur à bien été supprimé ✅ "
      );
    } catch (err) {
      next(err);
    }
  };

  getAllUsers = async (request, response, next) => {
    try {
      let usersReceived = await this.userService.getAllUsers();
      const userDto = new UserDto(usersReceived);
      const users = await userDto.mappingGetAllUsers(usersReceived);
      this.responseHandler(response, 200, users, 'Tous les utilisateur ✅');
    } catch (err) {
      next(err);
    }
  };

  getUserById = async (request, response, next) => {
    try {
      let userReceived = await this.userService.getUser(request.params.id);
      const userDto = new UserDto(userReceived);
      const user = await userDto.mappinGetUser(userReceived);
      this.responseHandler(response, 200, user);
    } catch (err) {
      next(err);
    }
  };

  resetPasswordByEmail = async (request, response, next) => {
    try {
      const user = await this.userService.resetPasswordByEmail(
        request.body.email
      );
      console.log(user.dataValues.role);
      if (user.dataValues.role === 'admin') {
        this.responseHandler(
          response,
          200,
          `Un e-mail a été envoyé à l'adresse e-mail`
        );
        // change method generic of templating email
        return await this.mailing.generateEmail(
          request.body.email,
          'Récuperation de votre mot de passe',
          'donation',
          {
            id: user.dataValues.id,
            firstname: user.dataValues.firstname,
            amount: '30000'
          }
        );
      }
      return this.responseHandler(
        response,
        400,
        'Il ne semble pas que vous soyez administateur pour accéder à cette requête ❌'
      );
    } catch (err) {
      next(err);
    }
  };
}

export default UserController;
