import notifier from 'node-notifier';

class ApiError extends Error {
  constructor(statusCode, message) {
    super();
    this.statusCode = statusCode;
    this.message = message;
  }
}

const handleError = (error, response) => {
  const message = error;
  const statusCode = error.statusCode ? error.statusCode : 500;
  console.log(`🔴 Gestion d'erreur 🔴 (❌ CODE : ${statusCode})=> `, message);

  notifier.notify({
    message: ` Code : ${statusCode} / ${message}`
  });

  response.status(statusCode).json({
    statusCode,
    message
  });
};

export { ApiError, handleError };
