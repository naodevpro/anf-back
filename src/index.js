import container from './container';

const application = container.resolve('server');
const config = container.resolve('config');

application.listen(config.server_port);
