class Server {
  constructor({
    express,
    cors,
    cookieParser,
    csrfMiddleware,
    routes,
    handleError
  }) {
    this.app = express();
    this.initializeBodyParsing(express);
    this.initializeApplicationCors(cors);
    this.initializeMiddlewares({ cookieParser, csrfMiddleware });
    this.initializeApplicationRouter(routes);
    this.app.use((err, request, response, next) => {
      handleError(err, response);
    });
  }

  initializeBodyParsing(express) {
    this.app.use(express.urlencoded({ extended: false }));
    this.app.use(express.json());
    this.app.use(express.static('uploads'));
  }

  initializeApplicationCors(cors) {
    this.app.use(
      cors({
        credentials: true,
        origin: [
          'http://localhost:3000',
          'http://alliancenouvellefrance.herokuapp.com',
          'https://alliancenouvellefrance.herokuapp.com',
          'http://164.132.58.128',
          'https://alliancenouvellefrance.fr',
          'http://alliancenouvellefrance.fr'
        ]
      })
    );
  }

  initializeMiddlewares({ cookieParser, csrfMiddleware }) {
    this.app.use(cookieParser());
    this.app.get('/csrf', csrfMiddleware, (req, res) => {
      res.status(200).json(req.csrfToken());
    });
  }

  initializeApplicationRouter(routes) {
    this.app.use('/api', routes);
  }

  listen(port) {
    this.app.listen(port || process.env.PORT, '0.0.0.0', () =>
      port
        ? console.log(
            `Application started on port : ${port || process.env.PORT} 💥`
          )
        : console.log('Application started on Heroku 🟣💥')
    );
  }
}

export default Server;
