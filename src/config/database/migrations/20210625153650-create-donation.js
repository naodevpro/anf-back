'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Donations', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4
      },
      userId: {
        type: Sequelize.UUID,
        references: {
          model: 'Users',
          key: 'id'
        }
      },
      nationality: {
        type: Sequelize.TEXT
      },
      adress: {
        type: Sequelize.TEXT
      },
      birthdate: {
        type: Sequelize.STRING
      },
      gender: {
        type: Sequelize.ENUM('female', 'masculin', 'other')
      },
      phone: {
        type: Sequelize.STRING
      },
      elected: {
        type: Sequelize.BOOLEAN
      },
      newsletter: {
        type: Sequelize.BOOLEAN
      },
      newsletterSms: {
        type: Sequelize.BOOLEAN
      },
      amount: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      updatedAt: {
        allowNull: false,
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Donations');
  }
};
